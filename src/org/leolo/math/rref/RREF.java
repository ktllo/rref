package org.leolo.math.rref;

import java.io.*;
import java.util.Random;
import java.util.Scanner;

public class RREF {
	public static void main(String... args){
		//Create sample TeX file
		PrintWriter out;
		try {
			out = new PrintWriter("output.tex");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.exit(1);
			return;
		}
		out.println("%% Auto generated TeX fiel for testing.");
		out.println("\\documentclass[a4paper,12pt]{article}");
		out.println("\\usepackage[utf8]{inputenc}");
		out.println("\\usepackage[margin=0.725in]{geometry}");
		out.println("\\usepackage{amsmath}");
		out.println("\\usepackage{amssymb}");
		out.println("\\usepackage{fancyhdr}");
		out.println("\\usepackage{amsfonts}");
		out.println("\\usepackage{color}");
		out.println("\\usepackage[british]{babel}");
		out.println("\\begin{document}");
		
		int [][] a = {{1,4,2,1},{-2,5,1,-2},{5,-1,2,3}};
		Matrix t = new Matrix(a);
		t = RREF.getInput();
		out.println("Finding the following matrix in Row Echelon Form and Reduced Row Echelon Form");
		out.println("\\begin{equation}");
		out.println(t);
		out.println("\\end{equation}");
		out.println(RREF.solve(t));
		out.println();
		out.println("Generated using program written by LO,Kam Tao Leo. Read output.tex for \\LaTeX source file.");
		out.println("\\end{document}");
		
		out.close();
		
		//Call pdflatex
		String [] call = {"pdflatex","-interaction=nonstopmode","output.tex"};
		ProcessBuilder pb = new ProcessBuilder(call);
		pb.redirectErrorStream(true);
		try {
			Process p = pb.start();
			BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
			while(true){
				String line = br.readLine();
				if(line == null) break;
				System.out.println(line);
			}
			p.waitFor();
		} catch (IOException | InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	private static String solve(Matrix m){
		StringBuffer sb = new StringBuffer();
		//FWD Elim
		while(true){
			//Check is finish
			if(m.hasVisableEntry()) break;
			int pivotCol = m.leftNonZeroCol();
			int destRow = m.topMostRow();
			int srcRow = m.getSrcRow(pivotCol);
			if(destRow != srcRow ){
				//Exchange
				sb.append("$r_"+(destRow+1)+"\\leftrightarrow r_"+(srcRow+1)+"$\\\\");
				m.interchange(destRow, srcRow);
				sb.append(m);
			}
			//Scale SELF to 1
			Fraction scale = m.get(destRow, pivotCol).inverse();
			if(!scale.isOne()){
				sb.append("$"+scale+"r_"+(destRow+1)+"$\\\\");
				m.scale(destRow, scale);
				sb.append(m);
			}
			//Replace ALL other row
			for(int i=destRow+1;i<m.numRow();i++){
				if(!m.get(i, pivotCol).isZero()){
					//Scaling need
					scale = m.get(i, pivotCol).multiply(-1);
					sb.append("$"+scale+"r_"+(destRow+1)+"+r_"+(i+1)+"$\\\\");
					m.replace(destRow, scale, i);
					sb.append(m);
				}
			}
			m.setInvisable(destRow);
			//break;
		}
		//BACK Elim
		//FROM last ROW
		for(int i=m.numRow()-1;i>=0;i--){
			int lead = m.findLeadingEntry(i);
			if(lead>0){
				//Action need
				for(int j=i-1;j>=0;j--){
					Fraction scale=m.get(j, lead).multiply(-1);
					if(scale.isZero()) continue;
					sb.append("$"+scale+"r_"+(i+1)+"+r_"+(j+1)+"$\\\\");
					m.replace(i, scale, j);
					sb.append(m);
				}
			}
		}
		return sb.toString();
	}
	
	private static Matrix getRandomMatrix(final int x,final int y){
		Random r = new Random();
		Fraction [][] array = new Fraction[x][y];
		for(int i=0;i<x;i++){
			for(int j=0;j<y;j++){
				array[i][j] = Fraction.parseFraction(""+r.nextInt(10)/*+"/"+r.nextInt(10000)*/);
			}
		}
		return new Matrix(array);
	}
	
	/*
	 * Get input from STDIN, message will be send to STDOUT
	 * 
	 * Store the data using Fraction
	 */
	private static Matrix getInput(){
		//Stage 1: Ask for number of ROW and COLUMN
		final int row = getInt("Number of rows : ");
		final int col = getInt("Number of columns : ");
		//Stage 1a: Create empty array
		Fraction [][] data = new Fraction[row][col];
		//Stage 2: Ask value ROW BY ROW
		for(int i=1;i<=row;i++){
			for(int j=1;j<=col;j++){
				final Fraction t = getFraction("Value of Row "+i+" Column "+j+": ");
				data[i-1][j-1]=t;
			}
		}
		//Stage 3: Construct the Matrix object
		return new Matrix(data);
	}
	
	private static int getInt(String message){
		Scanner s = new Scanner(System.in);
		while(true){
			System.out.print(message);
			String in = s.nextLine();
			try{
				int ret = Integer.parseInt(in);
				if(ret > 0)
					return ret;
				throw new NumberFormatException();
			}catch(NumberFormatException nfe){
				System.out.println("Invaild number!");
			}
		}
	}
	
	private static Fraction getFraction(String message){
		Scanner s = new Scanner(System.in);
		while(true){
			System.out.print(message);
			String in = s.nextLine();
			try{
				Fraction ret = Fraction.parseFraction(in);
				return ret;
			}catch(Exception nfe){
				System.out.println("Invaild number!");
			}
		}
	}
	
	
}
