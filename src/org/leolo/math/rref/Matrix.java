package org.leolo.math.rref;

public class Matrix {
	private Fraction [][] data;
	private boolean [] visable;
	
	protected Matrix(Fraction [][] array){
		data = array;
		visable = new boolean[data.length];
		for(int i=0;i<visable.length;i++){
			visable[i] = true;
		}
	}
	
	protected Matrix(int [][] array){
		data = new Fraction[array.length][array[0].length];
		for(int i=0;i<array.length; i++){
			for(int j=0;j<array[i].length;j++){
				data[i][j] = new Fraction(array[i][j]);
			}
		}
		visable = new boolean[data.length];
		for(int i=0;i<visable.length;i++){
			visable[i] = true;
		}
	}
	
	public void interchange(int r1,int r2){
		Fraction [] tmp = data[r1];
		data[r1] = data[r2];
		data[r2] = tmp;
	}
	
	public void scale(int row,Fraction scale){
		for(int i=0;i<data[row].length;i++){
			data[row][i] = data[row][i].multiply(scale);
		}
	}
	
	public void replace(int sRow, Fraction scale, int dRow){
		for(int i=0;i<data[dRow].length;i++){
			data[dRow][i] = data[dRow][i].add(data[sRow][i].multiply(scale));
		}
	}
	
	public String toString(){
		StringBuffer sb = new StringBuffer();
		sb.append("\\begin{equation}\\begin{bmatrix}\n");
		for(int i=0;i<data.length;i++){
			for(int j=0;j<data[i].length;j++){
				sb.append(data[i][j]);
				if(j+1 != data[i].length){
					sb.append(" & ");
				}
			}
			if(i+1 != data.length)
				sb.append("\\\\[0.3ex]");
			sb.append("\n");
		}
		sb.append("\\end{bmatrix}\\end{equation}");
		return sb.toString();
	}
	
	public boolean hasVisableEntry(){
		for(int i=0;i<visable.length;i++){
			if(visable[i]){
				for(int j=0;j<data[i].length;j++)
					if(!data[i][j].isZero())
						return false;
			}
		}
		return true;
	}
	
	public int leftNonZeroCol(){
		for(int i=0;i<data[0].length;i++){
			boolean isVaild = false;
			for(int j=0;j<data.length;j++){
				if(visable[j] && !data[j][i].isZero()){
					isVaild = true;
					break;
				}
			}
			if(isVaild)
				return i;
		}
		return -1;
	}
	
	public int topMostRow(){
		for(int i=0;i<visable.length;i++)
			if(visable[i])
				return i;
		return -1;
	}
	
	public Fraction get(int i,int j){
		return data[i][j];
	}
	
	public int getSrcRow(int col){
		for(int i=topMostRow();i<visable.length;i++)
			if(!data[i][col].isZero()) return i;
		return -1;
	}
	
	public int numRow(){
		return data.length;
	}
	
	
	
	
	
	public void setInvisable(int row){
		visable[row]=false;
	}
	
	public int findLeadingEntry(int row){
		for(int i=0;i<data[row].length;i++){
			if(!data[row][i].isZero())
				return i;
		}
		return -1;
	}
}
