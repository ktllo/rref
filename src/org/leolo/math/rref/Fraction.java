package org.leolo.math.rref;

public class Fraction {
	private long numerator;
	private long denominator;// +ve only
	
	private Fraction(){
		
	}
	
	public Fraction(int num){
		this.denominator = 1;
		this.numerator = num;
	}
	
	public Fraction(int numerator,int denominator){
		this.numerator = numerator;
		this.denominator = denominator;
	}
	
	public void pettify(){
		if(GCD(this.numerator, this.denominator) != 1){
			long gcd = GCD(this.numerator, this.denominator);
			this.numerator /= gcd;
			this.denominator /= gcd;
		}
		
		if(this.denominator < 0){
			//i.e. -ve
			this.numerator*=-1;
			this.denominator*=-1;
		}
	}
	
	public long GCD(long a, long b) {
	   if (b==0) return a;
	   return GCD(b,a%b);
	}
	
	public static Fraction parseFraction(String number){
		//Check is it in Z
		if(!number.contains("/")){
			//Number in Z
			Fraction f = new Fraction();
			f.denominator = 1;
			f.numerator = Long.parseLong(number);
			return f;
		}else{
			//Number in Q (Z still possible)
			String n = number.substring(0, number.indexOf("/"));
			String d = number.substring(number.indexOf("/")+1);
			Fraction f = new Fraction();
			f.denominator = Long.parseLong(d);
			f.numerator = Long.parseLong(n);
			f.pettify();
			return f;
		}
	}
	
	public Fraction add(Fraction n){
		Fraction m = this.clone();
		m.numerator *= n.denominator;
		n.numerator *= m.denominator;
		m.denominator *= n.denominator;
		n.denominator *=m.denominator;
		//m.deniminator == n.denominator
		m.numerator += n.numerator;
		m.pettify();
		return m;
	}
	
	public Fraction multiply(long l){
		Fraction f = this.clone();
		f.numerator *= l;
		f.pettify();
		return f;
	}
	
	public Fraction multiply(Fraction n){
		Fraction f = new Fraction();
		f.numerator = this.numerator * n.numerator;
		f.denominator = this.denominator * n.denominator;
		f.pettify();
		return f;
	}
	
	public Fraction inverse(){
		Fraction f = new Fraction();
		f.denominator = this.numerator;
		f.numerator = this.denominator;
		f.pettify();
		return f;
	}
	
	protected Fraction clone(){
		Fraction f = new Fraction();
		f.numerator = this.numerator;
		f.denominator = this.denominator;
		return f;
	}
	
	public String toString(){
		if(this.denominator!=1)
			if(numerator > 0)
				return "\\frac{"+this.numerator+"}{"+this.denominator+"}";
			else
				return "-\\frac{"+this.numerator*-1+"}{"+this.denominator+"}";
		else
			return ""+this.numerator;
	}
	
	public boolean equals(Fraction f){
		return f.numerator==this.numerator && f.denominator==this.denominator;
	}
	
	public boolean isZero(){
		return this.numerator == 0;
	}
	
	public boolean isOne(){
		return this.numerator == this.denominator;
	}
}
