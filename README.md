## 1. Prerequisite
+ pdflatex, with following package
    + amsmath
    + inputenc
    + geometry
    + amssymb
    + fancyhdr
    + amsfonts
    + color
    + babel

## 2. Oversized matrix
Only tested with matrix having up to 10 column. Matrix ***WILL NOT*** properly
rendered if exceed that size. ***MAY*** mix in future.

Matrix with too many ***ROW*** may not able to display, you will need to use
the generated **output.tex** and either edit it page size, or compile it as an
image

## 3. Others
Reports your bug at http://www.leolo.org/bugtracker/